import '../imports/api/commands'
import '../imports/api/registers'
import '../imports/api/flagRegister'
import '../imports/api/dataTypes'
import '../imports/startup/accounts-config'
import {Accounts} from 'meteor/accounts-base'
import {Meteor} from 'meteor/meteor'
import {Commands} from "../imports/api/commands"
import {Registers} from "../imports/api/registers"
import {FlagRegister} from "../imports/api/flagRegister"
import {DataTypes} from "../imports/api/dataTypes"

Meteor.startup(function () {
    if (Commands.find().count() === 0) {
        JSON.parse(Assets.getText("database/commands.json")).forEach(command => {
            Commands.insert(command)
        })
    }

    if (Registers.find().count() === 0) {
        JSON.parse(Assets.getText("database/registers.json")).forEach(register => {
            Registers.insert(register)
        })
    }

    if (FlagRegister.find().count() === 0) {
        JSON.parse(Assets.getText("database/flagRegister.json")).forEach(flag => {
            FlagRegister.insert(flag)
        })
    }

    if (DataTypes.find().count() === 0) {
        JSON.parse(Assets.getText("database/dataTypes.json")).forEach(dataType => {
            DataTypes.insert(dataType)
        })
    }

    if (Meteor.users.find().count() === 0) {
        Accounts.createUser({username: "ZhenyaUsenko", password: "es2pC28U"})
    }
})
