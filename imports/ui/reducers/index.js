import {Meteor} from "meteor/meteor"

const pages = ["/login", "/commands", "/registers", "/flag-register", "/data-types"]
const initialPath = pages.includes(document.location.pathname) ? document.location.pathname : "/"

const initialState = {
    previousPath: null,
    path: initialPath,
    exactPath: initialPath,
    fading: true,
    animating: true,
    mouseMovedFirst: false,
    disappearing: false,

    commands: [],
    registers: [],
    flagRegister: [],
    dataTypes: [],

    commandsSearch: "",
    registersSearch: "",
    flagRegisterSearch: "",
    dataTypesSearch: "",

    commandsChanges: null,
    registersChanges: null,
    flagRegisterChanges: null,
    dataTypesChanges: null,

    commandsSelecting: false,
    registersSelecting: false,
    flagRegisterSelecting: false,
    dataTypesSelecting: false,

    commandsReady: false,
    registersReady: false,
    flagRegisterReady: false,
    dataTypesReady: false,

    commandsFound: null,
    registersFound: null,
    flagRegisterFound: null,
    dataTypesFound: null,

    prepareForChanges: false,
    loginButtonAnimation: null,
    loggingIn: Meteor.loggingIn(),
    user: Meteor.user()
}

export default function reducer(state = initialState, action) {

    switch (action.type) {
        case 'SET_DIRECTORY_DATA': {
            const found = state[action.payload.directory + "Search"] !== "" ?
                state[action.payload.directory].find(document =>
                    ~document[action.payload.searchFieldDB].toLowerCase()
                        .indexOf(state[action.payload.directory + "Search"].toLowerCase())) :
                null

            return {
                ...state,
                [action.payload.directory]: action.payload.value,
                [action.payload.directory + "Found"]: found,
                [action.payload.directory + "Ready"]: action.payload.ready
            }
        }

        case 'SET_DIRECTORY_SELECTING': {
            return {...state, [action.payload.directory + "Selecting"]: action.payload.value}
        }

        case 'SET_DIRECTORY_EDITING':
            return {...state, [action.payload.directory + "Changes"]: action.payload.value}

        case 'SET_DIRECTORY_CHANGES': {
            return {
                ...state,
                [action.payload.directory + "Changes"]: {
                    ...state[action.payload.directory + "Changes"],
                    [action.payload.propertyName]: action.payload.value
                }
            }
        }

        case 'SET_LOGGING_IN':
            return {...state, loggingIn: action.payload}

        case 'SET_USER':
            return {...state, user: action.payload, editing: action.payload ? state.editing : false}


        case 'SET_PAGE_PATH':
            return {
                ...state,
                disappearing: true,
                animating: true,
                mouseMovedFirst: false,
                searching: false,
                editing: false,
                previousPath: state.path,
                path: action.payload,
                exactPath: state.path,
                fading: action.payload === "/login" || state.path === "/login"
            }

        case 'SET_PAGE_ANIMATING':
            return {...state, animating: action.payload}

        case 'SET_LOGIN_BUTTON_ANIMATION':
            return {
                ...state,
                loginButtonAnimation: action.payload && !state.animating && state.exactPath === "/login" ?
                    action.payload : null
            }

        case 'SET_PAGE_MOUSE_MOVED_FIRST':
            return {...state, mouseMovedFirst: action.payload}

        case 'SET_PAGE_DISAPPEARING':
            return {...state, disappearing: action.payload, exactPath: action.payload ? state.previousPath : state.path}

        case 'SET_PAGE_SEARCHING':
            return {...state, searching: action.payload}

        case 'SET_SEARCH_STRING': {
            const found = state[action.payload.directory + "Search"] !== "" ?
                state[action.payload.directory].find(document =>
                    ~document[action.payload.searchFieldDB].toLowerCase()
                        .indexOf(state[action.payload.directory + "Search"].toLowerCase())) :
                null

            return {
                ...state,
                [action.payload.directory + "Found"]: found,
                [action.payload.directory + "Search"]: action.payload.value
            }
        }

        case 'MUTATE_PREPARE_FOR_CHANGES':
            state.prepareForChanges = action.payload
            return state

        default:
            return state
    }

}