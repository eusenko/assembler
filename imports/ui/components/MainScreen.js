import React, {Component} from 'react'
import classnames from 'classnames'
import {connect} from "react-redux"

class MainScreen extends Component {

    render() {
        const navBlock = classnames(
            "nav-block",
            {
                animating: this.props.animating,
                disappearing: this.props.disappearing,
                fading: this.props.fading
            }
        )

        const setAnimating = this.props.animating ?
            this.props.disappearing ?
                () => this.props.actions.setPageDisappearing(false) :
                () => this.props.actions.setPageAnimating(false) :
            null

        const setCommandsLink = !this.props.animating ?
            () => this.props.actions.setPagePath("/commands", true) :
            null

        const setRegistersLink = !this.props.animating ?
            () => this.props.actions.setPagePath("/registers", true) :
            null

        const setFlagRegisterLink = !this.props.animating ?
            () => this.props.actions.setPagePath("/flag-register", true) :
            null

        const setDataTypesLink = !this.props.animating ?
            () => this.props.actions.setPagePath("/data-types", true) :
            null

        return (
            <div className="main">
                <div className="nav-blocks-container">
                    <div
                        onAnimationEnd={setAnimating}
                        className={`nav-block-top-left ${navBlock}`}
                        onClick={setCommandsLink}
                    >
                        <div className="cursor-shadow"/>
                        <div className="inner">
                            <div className="text-shadow">КОМАНДЫ АССЕМБЛЕРА</div>
                            <div className="cursor-shadow show-always"/>
                            <div className={`text ${navBlock}`}>КОМАНДЫ АССЕМБЛЕРА</div>
                        </div>
                    </div>
                    <div className={`nav-block-top-right ${navBlock}`} onClick={setFlagRegisterLink}>
                        <div className="cursor-shadow"/>
                        <div className="inner">
                            <div className="text-shadow">РЕГИСТР ФЛАГОВ</div>
                            <div className="cursor-shadow show-always"/>
                            <div className={`text ${navBlock}`}>РЕГИСТР ФЛАГОВ</div>
                        </div>
                    </div>
                    <div className={`nav-block-bottom-left ${navBlock}`} onClick={setRegistersLink}>
                        <div className="cursor-shadow"/>
                        <div className="inner">
                            <div className="text-shadow">РЕГИСТРЫ ПРОЦЕССОРА</div>
                            <div className="cursor-shadow show-always"/>
                            <div className={`text ${navBlock}`}>РЕГИСТРЫ ПРОЦЕССОРА</div>
                        </div>
                    </div>
                    <div className={`nav-block-bottom-right ${navBlock}`} onClick={setDataTypesLink}>
                        <div className="cursor-shadow"/>
                        <div className="inner">
                            <div className="text-shadow">ТИПЫ ДАННЫХ</div>
                            <div className="cursor-shadow show-always"/>
                            <div className={`text ${navBlock}`}>ТИПЫ ДАННЫХ</div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        animating: state.animating,
        fading: state.fading,
        disappearing: state.disappearing
    }
}

export default connect(mapStateToProps)(MainScreen)
