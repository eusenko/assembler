import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import * as actions from '../actions/PageActions'
import MainScreen from "./MainScreen"
import Login from './Login'
import LoginButton from './LoginButton'
import Directory from './Directoty'
import classnames from "classnames"

class App extends Component {

    shadowPositionHandler = e => {
        const fontSize = window.getComputedStyle(document.documentElement).fontSize.replace("px", "")
        const cursorShadows = document.getElementsByClassName("cursor-shadow")

        for (const shadow of cursorShadows) {
            const parentPosition = shadow.parentNode.getBoundingClientRect()
            shadow.style.transform = `translate(
            ${(e.clientX - parentPosition.left) / fontSize + "rem"}, 
            ${(e.clientY - parentPosition.top) / fontSize + "rem"}
            )`
        }
    }

    componentDidMount() {
        history.replaceState(null, null, this.props.exactPath)

        window.addEventListener("popstate", () => {
            this.props.actions.setPagePath(document.location.pathname)
        })
    }

    render() {
        const mainContainer = classnames("main-container", {"mouse-moved": this.props.mouseMovedFirst})

        const setMouseMoveHandler = !this.props.animating ?
            !this.props.mouseMovedFirst ?
                e => {
                    this.shadowPositionHandler(e)
                    this.props.actions.setPageMouseMovedFirst(true)
                } :
                this.shadowPositionHandler :
            null

        const getRouteContent = () => {
            switch (this.props.exactPath) {
                case "/":
                    return <MainScreen actions={this.props.actions}/>

                case "/login":
                    return <Login actions={this.props.actions}/>

                default:
                    return <Directory actions={this.props.actions}/>
            }
        }


        return (
            <div className={mainContainer} onMouseMove={setMouseMoveHandler}>
                <LoginButton actions={this.props.actions}/>
                {getRouteContent()}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        exactPath: state.exactPath,
        animating: state.animating,
        mouseMovedFirst: state.mouseMovedFirst
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)