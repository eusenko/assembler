import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Meteor} from "meteor/meteor"

class DirectoryButton extends Component {

    render() {
        const getClickHandler = () => {
            if (this.props.animating) return null

            switch (this.props.className) {
                case "back-button":
                    return () => {
                        this.props.previousPath ?
                            history.back() :
                            this.props.actions.setPagePath("/", true)
                    }

                case "edit-reset-button":
                    return () => this.props.actions.setPageEditing(!this.props.editing)

                case "search-delete-button":
                    return this.props.editing ?
                        () => Meteor.call('commands.remove', textEditorData[0]._id) :
                        () => this.props.actions.setPageSearching(!this.props.searching)

                case "save-new-button":
                    return this.props.editing ?
                        textEditorData.length ?
                            () => {
                                Meteor.call(
                                    'commands.update',
                                    textEditorData[0]._id,
                                    {
                                        designation: this.textEditors[0].editableContentNode.textContent,
                                        name: this.textEditors[1].editableContentNode.textContent,
                                        syntax: this.textEditors[2].editableContentNode.textContent,
                                        type: this.textEditors[3].editableContentNode.textContent,
                                        variants: this.textEditors[4].editableContentNode.textContent.split("\n"),
                                        description: this.textEditors[5].editableContentNode.textContent,
                                        features: this.textEditors[6].editableContentNode.textContent
                                    }
                                )
                            } :
                            null :
                        () => {
                            Meteor.call('commands.insert', {})
                        }
            }
        }

        return (
            this.props.user &&
            <div className={this.props.className} onClick={getClickHandler()} style={this.props.style}>
                {this.props.content[this.props.editing ? 0 : 1]}
                <div className="cursor-shadow show-always"/>
            </div>
        )
    }
}

function mapStateToProps(state, ownProps) {
    return {
        animating: state.animating,
        previousPath: ownProps.dependencies.includes("previousPath") ? state.previousPath : true,
        editing: ownProps.dependencies.includes("editing") ? state.editing : true,
        searching: ownProps.dependencies.includes("searching") ? state.searching : true,
        user: ownProps.dependencies.includes("user") ? state.user : true
    }
}

export default connect(mapStateToProps)(DirectoryButton)