import React, {Component} from 'react'
import {Meteor} from 'meteor/meteor'
import classnames from 'classnames'
import {connect} from "react-redux"

class Login extends Component {

    render() {
        const loginFieldsContainer = classnames(
            "container",
            {animating: this.props.animating, disappearing: this.props.disappearing}
        )

        const setAnimating = this.props.animating ?
            this.props.disappearing ?
                () => this.props.actions.setPageDisappearing(false) :
                () => this.props.actions.setPageAnimating(false) :
            null

        const setSubmitHandler = !this.props.animating ?
            e => {
                e && e.preventDefault()
                !this.props.loggingIn && !this.props.user &&
                Meteor.loginWithPassword(
                    document.forms[0].elements.login.value,
                    document.forms[0].elements.password.value,
                    err => this.props.actions.setLoginButtonAnimation(
                        err ?
                        "login-fail .6s linear" :
                        "login-success .6s linear"
                    )
                )
            } :
            null

        return (
            <div className="login">
                <div className={loginFieldsContainer} onAnimationEnd={setAnimating}>
                    <div className="cursor-shadow"/>
                    <div onClick={setSubmitHandler} className="inner">
                        <form
                            onClick={e => e.stopPropagation()}
                            className="login-fields"
                            onSubmit={setSubmitHandler}
                            noValidate
                        >
                            <div onClick={setSubmitHandler} className="label-container">
                                <div className="cursor-shadow show-always"/>
                                <label onClick={e => e.stopPropagation()} htmlFor="login">Логин</label>
                            </div>
                            <div className="field">
                                <input id="login" type="text" name="login" spellCheck="false"/>
                            </div>
                            <div onClick={setSubmitHandler} className="label-container">
                                <div className="cursor-shadow show-always"/>
                                <label onClick={e => e.stopPropagation()} htmlFor="password">Пароль</label>
                            </div>
                            <div className="field">
                                <input id="password" type="password" name="password" spellCheck="false"/>
                            </div>
                            <button type="submit"/>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        animating: state.animating,
        disappearing: state.disappearing,
        loggingIn: state.loggingIn,
        user: state.user
    }
}

export default connect(mapStateToProps)(Login)
