import React, {Component} from 'react'
import classnames from 'classnames'
import TextEditor from "./TextEditor.js"
import DirectoryButton from "./DirectoryButton"
import {connect} from "react-redux"

class Directory extends Component {

    render() {
        const directoryContainer = classnames(
            "directory-container",
            {
                [this.props.exactPath.replace("/", "")]: true,
                animating: this.props.animating,
                disappearing: this.props.disappearing,
                fading: this.props.fading
            }
        )

        const setAnimating = this.props.animating ?
            this.props.disappearing ?
                () => this.props.actions.setPageDisappearing(false) :
                () => this.props.actions.setPageAnimating(false) :
            null
        
        let textEditorArray = [],
            topContent = [],
            bottomContent = [],
            dataFieldNames,
            titles,
            placeholder,
            directory,
            editorsCount

        switch (this.props.exactPath) {
            case "/data-types":
                editorsCount = {top: 1, bottom: 1}
                directory = "dataTypes"
                placeholder = "Введите команду..."
                titles = ["Название", "Описание"]
                dataFieldNames = ["name", "description"]
                break

            case "/flag-register":
                editorsCount = {top: 4, bottom: 2}
                directory = "flagRegister"
                placeholder = "Введите флаг..."
                titles = ["Обозначение", "Название", "Номер бита", "Тип", "Описание", "Особенности"]
                dataFieldNames = ["designation", "name", "bitNumber", "type", "description", "features"]
                break

            case "/registers":
                editorsCount = {top: 4, bottom: 1}
                directory = "registers"
                placeholder = "Введите регистр..."
                titles = ["Обозначение", "Название", "Разрядность", "Тип", "Описание"]
                dataFieldNames = ["designation", "name", "capacity", "type", "description"]
                break

            default:
                editorsCount = {top: 4, bottom: 3}
                directory = "commands"
                placeholder = "Введите тип данных..."
                titles = ["Обозначение", "Название", "Синтаксис", "Тип", "Варианты", "Описание", "Особенности"]
                dataFieldNames = ["designation", "name", "syntax", "type", "variants", "description", "features"]
        }

        const textEditor = {
            actions: this.props.actions,
            editorsCount: editorsCount.top + editorsCount.bottom,
            searchFieldDB: dataFieldNames[0],
            directory
        }

        for (let i = 0; i < textEditor.editorsCount; i++) {
            const handleAnimationEnd = !this.props.fading &&
                (i === 0 && this.props.disappearing || i === textEditor.editorsCount - 1 && !this.props.disappearing)

            textEditorArray.push({
                ...textEditor,
                key: textEditor.directory + i,
                position: i,
                search: i === 0,
                propertyName: dataFieldNames[i],
                title: titles[i],
                placeholder: i === 0 ? placeholder : null,
                handleAnimationEnd
            })
        }

        for (let i = 1; i <= editorsCount.top * 2 - 1; i++) {
            topContent.push(i % 2 ? <TextEditor {...textEditorArray.shift()}/> : <div className="border"/>)
        }

        for (let i = 1; i <= editorsCount.bottom * 2 - 1; i++) {
            bottomContent.push(i % 2 ? <TextEditor {...textEditorArray.shift()}/> : <div className="border"/>)
        }

        let directoryButtonArray = [
            {
                key: "back-button",
                dependencies: ["previousPath"],
                className: "back-button",
                content: ["&#x2B06;"],
                style: {top: 0, right: 0},
                actions: this.props.actions

            },
            {
                key: "search-delete-button",
                dependencies: ["user", "editing", "searching"],
                className: "search-delete-button",
                content: ["D", "F"],
                style: {top: 0, left: 0},
                actions: this.props.actions
            },
            {
                key: "edit-reset-button",
                dependencies: ["user", "editing"],
                className: "edit-reset-button",
                content: ["R", "E"],
                style: {bottom: 0, left: 0},
                actions: this.props.actions
            },
            {
                key: "save-new-button",
                dependencies: ["user", "editing"],
                className: "save-new-button",
                content: ["S", "N"],
                style: {bottom: 0, right: 0},
                actions: this.props.actions
            }
        ]

        directoryButtonArray = directoryButtonArray.map(button => <DirectoryButton {...button}/>)


        return (
            <div className="directory">
                <div className={directoryContainer} onAnimationEnd={setAnimating}>
                    <div className="cursor-shadow"/>
                    <div className="directory-inner">
                        <div className="container">
                            <div className="top-content">
                                {topContent}
                            </div>
                            <div className="divider"/>
                            <div className="bottom-content">
                                {bottomContent}
                            </div>
                        </div>
                        {directoryButtonArray}
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        exactPath: state.exactPath,
        animating: state.animating,
        fading: state.fading,
        disappearing: state.disappearing
    }
}

export default connect(mapStateToProps)(Directory)
