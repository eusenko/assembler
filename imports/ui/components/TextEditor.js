import React, {Component} from 'react'
import classnames from 'classnames'
import {connect} from "react-redux"

class TextEditor extends Component {

    editableContentNode = React.createRef()

    getSnapshotBeforeUpdate() {
        return {
            selectionStart: this.editableContentNode.current.selectionStart,
            selectionEnd: this.editableContentNode.current.selectionEnd
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        this.editableContentNode.current.setSelectionRange(snapshot.selectionStart, snapshot.selectionEnd)
    }


    render() {
        const animationDelay = this.props.animating && !this.props.fading ?
            {
                animationDelay: `${0.06 * (this.props.disappearing ?
                    this.props.editorsCount - this.props.position :
                    this.props.position)}s`
            } :
            null

        const content = classnames(
            "content",
            {
                animating: this.props.animating && !this.props.fading,
                disappearing: this.props.disappearing,
            }
        )

        const editableContent = classnames(
            "editable-content",
            {
                search: this.props.search,
                "show-placeholder": this.props.search &&
                this.props.searchStringUI === "" &&
                !this.props.editing
            }
        )

        const setAnimating = this.props.animating ?
            this.props.handleAnimationEnd ?
                this.props.disappearing ?
                    e => {
                        e.stopPropagation()
                        this.props.actions.setPageDisappearing(false)
                    } :
                    e => {
                        e.stopPropagation()
                        this.props.actions.setPageAnimating(false)
                    } :
                e => e.stopPropagation() :
            null

        const inputHandler = this.props.search && !this.props.editing ?
            () => {
                const textContent = this.editableContentNode.current.textContent
                if (textContent !== props.searchStringUI) {
                    props.actions.setSearchString({
                        searchFieldDB: props.searchFieldDB,
                        directory: props.directory,
                        value: textContent
                    })
                } else {
                    for (const child of this.editableContentNode.current.children) {
                        child.remove()
                    }
                }
            } :
            null

        const pasteHandler = e => {
            e.preventDefault()
            const text = e.clipboardData.getData('text/plain')
            window.document.execCommand('insertText', false, text)
        }

        let dataBefore = "", dataAfter = ""
        if (this.props.search && !this.props.editing) {
            [dataBefore, ...dataAfter] = this.props.text.toLowerCase()
                .split(this.props.searchStringUI.toLowerCase())
            dataAfter = dataAfter.join(this.props.searchStringUI.toLowerCase())
        }

        return (
            <div className="text-editor">
                <div className="title">
                    {this.props.title}
                    <div className="cursor-shadow show-always"/>
                </div>
                <div className={content} onAnimationEnd={setAnimating} style={animationDelay}>
                    <div className="wrapper">
                        <div
                            ref={this.editableContentNode}
                            onInput={inputHandler}
                            onPaste={pasteHandler}
                            data-before={dataBefore}
                            data-after={dataAfter}
                            data-placeholder={this.props.placeholder}
                            className={editableContent}
                            style={this.props.search ? {textTransform: "uppercase"} : null}
                            contentEditable={!this.props.animating && (this.props.editing || this.props.search)}
                            suppressContentEditableWarning
                            spellCheck={false}
                        >
                            {text}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state, ownProps) {
    return {
        fading: state.fading,
        editing: state.editing,
        searchStringUI: state[ownProps.directory + "Search"],
        animating: state.animating,
        disappearing: state.disappearing,
        user: state.user,
        text: state[ownProps.directory + "Found"] ? state[ownProps.directory + "Found"][ownProps.propertyName] : ""
    }
}

export default connect(mapStateToProps)(TextEditor)
