export function setPagePath(payload, pushToBrowserHistory) {
    pushToBrowserHistory && history.pushState(null, null, payload)

    return {
        type: 'SET_PAGE_PATH',
        payload
    }
}

export function setPageAnimating(payload) {
    return {
        type: 'SET_PAGE_ANIMATING',
        payload
    }
}

export function setLoginButtonAnimation(payload) {
    return {
        type: 'SET_LOGIN_BUTTON_ANIMATION',
        payload
    }
}

export function setPageMouseMovedFirst(payload) {
    return {
        type: 'SET_PAGE_MOUSE_MOVED_FIRST',
        payload
    }
}

export function setPageDisappearing(payload) {
    return {
        type: 'SET_PAGE_DISAPPEARING',
        payload
    }
}

export function setPageSearching(payload) {
    return {
        type: 'SET_PAGE_SEARCHING',
        payload
    }
}

export function setSearchString(payload) {
    return {
        type: 'SET_SEARCH_STRING',
        payload
    }
}

export function setDirectoryData(payload) {
    return {
        type: 'SET_DIRECTORY_DATA',
        payload
    }
}

export function setDirectorySelecting(payload) {
    return {
        type: 'SET_DIRECTORY_SELECTING',
        payload
    }
}

export function setDirectoryEditing(payload) {
    return {
        type: 'SET_DIRECTORY_EDITING',
        payload
    }
}

export function setDirectoryChanges(payload) {
    return {
        type: 'SET_DIRECTORY_CHANGES',
        payload
    }
}

export function setLoggingIn(payload) {
    return {
        type: 'SET_LOGGING_IN',
        payload
    }
}

export function setUser(payload) {
    return {
        type: 'SET_USER',
        payload
    }
}

export function MUTATE_prepareForChanges(payload) {
    return {
        type: 'MUTATE_PREPARE_FOR_CHANGES',
        payload
    }
}