import {Meteor} from 'meteor/meteor'
import {Mongo} from 'meteor/mongo'
import {check} from 'meteor/check'
import SimpleSchema from 'simpl-schema';

export const Registers = new Mongo.Collection('registers')

Registers.schema = new SimpleSchema({
    designation: {type: String, defaultValue: 'Untitled', unique: true},
    name: {type: String, defaultValue: '', optional: true},
    capacity: {type: String, defaultValue: '', optional: true},
    description: {type: String, defaultValue: '', optional: true},
    type: {type: String, defaultValue: '', optional: true}
})

Registers.attachSchema(Registers.schema)


if (Meteor.isServer) {
    Meteor.publish('registers', function registersPublication() {
        return this.userId ?
            Registers.find() :
            Registers.find({
                $and: [
                    {designation: {$ne: 'Untitled'}},
                    {name: {$ne: ''}},
                    {capacity: {$ne: ''}},
                    {description: {$ne: ''}},
                    {type: {$ne: ''}}
                ]
            })
    })
}

Meteor.methods({
    'registers.insert'(register) {
        if (!this.userId) {
            throw new Meteor.Error('not-authorized')
        }

        Registers.insert(register)
    },
    'registers.remove'(registerId) {
        check(registerId, String)

        if (!this.userId) {
            throw new Meteor.Error('not-authorized')
        }

        Registers.remove(registerId)
    },
    'registers.upsert'(registerId, register) {
        check(registerId, String)

        if (!this.userId) {
            throw new Meteor.Error('not-authorized')
        }

        Registers.upsert(registerId, {$set: register})
    }
})
