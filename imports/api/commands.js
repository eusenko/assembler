import {Meteor} from 'meteor/meteor'
import {Mongo} from 'meteor/mongo'
import {check} from 'meteor/check'
import SimpleSchema from 'simpl-schema';

export const Commands = new Mongo.Collection('commands')

Commands.schema = new SimpleSchema({
    designation: {
        type: String,
        autoValue: function() {
            console.log("inssssssssssssssssssseeeeeeeeeeeeeeeeeeeeeeert")
            if (this.isInsert) {
                return "Untitled"
            }
        },
        unique: true
    },
    name: {
        type: String,
        autoValue: function() {
            if (this.isInsert) {
                return ""
            }
        },
        optional: true
    },
    syntax: {
        type: String,
        autoValue: function() {
            if (this.isInsert) {
                return ""
            }
        },
        optional: true
    },
    description: {
        type: String,
        autoValue: function() {
            if (this.isInsert) {
                return ""
            }
        },
        optional: true
    },
    features: {
        type: String,
        autoValue: function() {
            if (this.isInsert) {
                return ""
            }
        },
        optional: true},
    type: {
        type: String,
        autoValue: function() {
            if (this.isInsert) {
                return ""
            }
        },
        optional: true
    },
    variants: {
        type: Array,
        autoValue: function() {
            if (this.isInsert) {
                return []
            }
        },
    },
    'variants.$': {type: String}
})

Commands.attachSchema(Commands.schema, {transform: true})


if (Meteor.isServer) {
    Meteor.publish('commands', function commandsPublication() {
        return this.userId ?
            Commands.find() :
            Commands.find({
                $and: [
                    {designation: {$ne: ''}},
                    {name: {$ne: ''}},
                    {syntax: {$ne: ''}},
                    {description: {$ne: ''}},
                    {features: {$ne: ''}},
                    {type: {$ne: ''}}
                ]
            })
    })
}

Meteor.methods({
    'commands.insert'(command) {
        if (!this.userId) {
            throw new Meteor.Error('not-authorized')
        }

        Commands.insert(command)
    },
    'commands.remove'(commandId) {
        check(commandId, String)

        if (!this.userId) {
            throw new Meteor.Error('not-authorized')
        }

        Commands.remove(commandId)
    },
    'commands.upsert'(commandId, command) {
        check(commandId, String)

        if (!this.userId) {
            throw new Meteor.Error('not-authorized')
        }

        Commands.upsert(commandId, {$set: command})
    }
})
