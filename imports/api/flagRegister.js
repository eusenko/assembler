import {Meteor} from 'meteor/meteor'
import {Mongo} from 'meteor/mongo'
import {check} from 'meteor/check'
import SimpleSchema from 'simpl-schema';

export const FlagRegister = new Mongo.Collection('flagRegister')

FlagRegister.schema = new SimpleSchema({
    designation: {type: String, defaultValue: 'Untitled', unique: true},
    bitNumber: {type: String, defaultValue: '', optional: true},
    name: {type: String, defaultValue: '', optional: true},
    type: {type: String, defaultValue: '', optional: true},
    description: {type: String, defaultValue: '', optional: true},
    features: {type: String, defaultValue: '', optional: true}
})

FlagRegister.attachSchema(FlagRegister.schema)


if (Meteor.isServer) {
    Meteor.publish('flagRegister', function flagRegisterPublication() {
        return this.userId ?
            FlagRegister.find() :
            FlagRegister.find({
                $and: [
                    {designation: {$ne: 'Untitled'}},
                    {bitNumber: {$ne: ''}},
                    {name: {$ne: ''}},
                    {type: {$ne: ''}},
                    {description: {$ne: ''}},
                    {features: {$ne: ''}}
                ]
            })
    })
}

Meteor.methods({
    'flagRegister.insert'(flagRegister) {
        if (!this.userId) {
            throw new Meteor.Error('not-authorized')
        }

        FlagRegister.insert(flagRegister)
    },
    'flagRegister.remove'(flagId) {
        check(flagId, String)

        if (!this.userId) {
            throw new Meteor.Error('not-authorized')
        }

        FlagRegister.remove(flagId)
    },
    'flagRegister.upsert'(flagId, flag) {
        check(flagId, String)

        if (!this.userId) {
            throw new Meteor.Error('not-authorized')
        }

        FlagRegister.upsert(flagId, {$set: flag})
    }
})
