import {Meteor} from 'meteor/meteor'
import {Mongo} from 'meteor/mongo'
import {check} from 'meteor/check'
import SimpleSchema from 'simpl-schema';

export const DataTypes = new Mongo.Collection('dataTypes')

DataTypes.schema = new SimpleSchema({
    name: {type: String, defaultValue: 'Untitled', unique: true},
    description: {type: String, defaultValue: '', optional: true}
})

DataTypes.attachSchema(DataTypes.schema)

if (Meteor.isServer) {
    Meteor.publish('dataTypes', function dataTypesPublication() {
        return this.userId ?
            DataTypes.find() :
            DataTypes.find({
                $and: [
                    {name: {$ne: 'Untitled'}},
                    {description: {$ne: ''}}
                ]
            })
    })
}

Meteor.methods({
    'dataTypes.insert'(dataType) {
        if (!this.userId) {
            throw new Meteor.Error('not-authorized')
        }

        DataTypes.insert(dataType)
    },
    'dataTypes.remove'(dataTypeId) {
        check(dataTypeId, String)

        if (!this.userId) {
            throw new Meteor.Error('not-authorized')
        }

        DataTypes.remove(dataTypeId)
    },
    'dataTypes.upsert'(dataTypeId, dataType) {
        check(dataTypeId, String)

        if (!this.userId) {
            throw new Meteor.Error('not-authorized')
        }

        DataTypes.upsert(dataTypeId, {$set: dataType})
    }
})
