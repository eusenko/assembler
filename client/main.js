import React from 'react'
import {Provider} from 'react-redux'
import {Meteor} from 'meteor/meteor'
import {render} from 'react-dom'
import configureStore from '../imports/ui/store/configureStore'
import App from '../imports/ui/components/App.js'
import {Registers} from "../imports/api/registers"
import {FlagRegister} from "../imports/api/flagRegister"
import {DataTypes} from "../imports/api/dataTypes"
import {Commands} from "../imports/api/commands"
import {setDirectoryData} from "../imports/ui/actions/PageActions"
import {setLoggingIn} from "../imports/ui/actions/PageActions"
import {setUser} from "../imports/ui/actions/PageActions"

Meteor.startup(() => {
    const store = configureStore()

    let skipFirstCommandsAutorun = true
    Tracker.autorun(() => {
        const commandsSubscription = Meteor.subscribe('commands')
        const commands = Commands.find({}, {sort: ['type','designation']}).fetch()

        if (skipFirstCommandsAutorun) {
            skipFirstCommandsAutorun = false
        } else {
            store.dispatch(setDirectoryData({
                searchFieldDB: "designation",
                directory: "commands",
                value: commands,
                ready: commandsSubscription.ready()
            }))
        }
    })

    let skipFirstRegistersAutorun = true
    Tracker.autorun(() => {
        const registersSubscription = Meteor.subscribe('registers')
        const registers = Registers.find({}, {sort: ['type','designation']}).fetch()

        if (skipFirstRegistersAutorun) {
            skipFirstRegistersAutorun = false
        } else {
            store.dispatch(setDirectoryData({
                searchFieldDB: "designation",
                directory: "registers",
                value: registers,
                ready: registersSubscription.ready()
            }))
        }
    })

    let skipFirstFlagRegisterAutorun = true
    Tracker.autorun(() => {
        const flagRegisterSubscription = Meteor.subscribe('flagRegister')
        const flagRegister = FlagRegister.find({}, {sort: ['bitNumber']}).fetch()

        if (skipFirstFlagRegisterAutorun) {
            skipFirstFlagRegisterAutorun = false
        } else {
            store.dispatch(setDirectoryData({
                searchFieldDB: "designation",
                directory: "flagRegister",
                value: flagRegister,
                ready: flagRegisterSubscription.ready()
            }))
        }
    })

    let skipFirstDataTypesAutorun = true
    Tracker.autorun(() => {
        const dataTypesSubscription = Meteor.subscribe('dataTypes')
        const dataTypes = DataTypes.find({}, {sort: ['name']}).fetch()

        if (skipFirstDataTypesAutorun) {
            skipFirstDataTypesAutorun = false
        } else {
            store.dispatch(setDirectoryData({
                searchFieldDB: "name",
                directory: "dataTypes",
                value: dataTypes,
                ready: dataTypesSubscription.ready()
            }))
        }
    })

    let skipFirstLoggingInAutorun = true
    Tracker.autorun(() => {
        const loggingIn = Meteor.loggingIn()

        if (skipFirstLoggingInAutorun) {
            skipFirstLoggingInAutorun = false
        } else {
            store.dispatch(setLoggingIn(loggingIn))
        }
    })

    let skipFirstUserAutorun = true
    Tracker.autorun(() => {
        const user = Meteor.user()

        if (skipFirstUserAutorun) {
            skipFirstUserAutorun = false
        } else {
            store.dispatch(setUser(user))
        }
    })

    render(
        <Provider store={store}>
            <App/>
        </Provider>,
        document.getElementById('render-target')
    )
})
